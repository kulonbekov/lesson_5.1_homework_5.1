package com.company;

import java.util.Random;

public class Boss {

    private int bossZdorovia;
    private int bossUdar;
    private int bossSposobnost;


    public int getBossZdorovia() {
        return bossZdorovia;
    }

    public void setBossZdorovia(int bossZdorovia) {
        this.bossZdorovia = bossZdorovia;
    }

    public int getBossUdar() {
        return bossUdar;
    }

    public void setBossUdar(int bossUdar) {
        this.bossUdar = bossUdar;
    }

    public int getBossSposobnost() {
        return bossSposobnost;
    }

    public static int changeBossDefence() {
        Random r = new Random();
         int randomNum = r.nextInt(3) + 1;
            return randomNum;
    }

    public void setBossSposobnost(int bossSposobnost) {
        this.bossSposobnost = bossSposobnost + changeBossDefence();
    }
}
